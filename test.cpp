#include <iostream>
#include <vector>

int main() {
    std::vector<int> fib(20);
    fib[1] = 1;
    fib[2] = 2;
    for (int i = 3; i < fib.size(); ++i) {
	fib[i] = fib[i - 1] + fib[i - 2];
    }

    std::cout << fib[19] << std::endl;
}
